# Decluttering

A short interactive storyworld about 30 minutes of decluttering.

All code is licensed under the Affero GNU Public License version 3 or (at your option) any later version of the Affero GNU Pubic License. See LICENSE for more details.

All content under the `data` directory is released under a Creative Commons CC-BY-SA 4.0 International license.
